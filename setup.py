from setuptools import setup, find_packages

requires = [
    'uvicorn[standard]==0.18.3',
    'fastapi==0.85.0',
    'SQLAlchemy==1.4.41',
    'asyncpg==0.26.0',
    'alembic==1.8.1',
    'httpx==0.23.3',
]

tests_requires = [
    'devtools',
    'pytest==7.1.3',
    'pytest-asyncio==0.19.0',
]

setup(
    name='bet-maker',
    version='0.0',
    description='Bet maker Api',
    classifiers=[
        'Programming Language :: Python',
        'Framework :: FastAPI',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: ASGI :: Application',
    ],
    author='Danila Postnikov',
    author_email='luginich.danil@gmail.com',
    keywords='web fastapi pylons',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    extras_require={
        'testing': tests_requires,
    },
    install_requires=requires,
    entry_points={
        'console_scripts': [],
    },
)
