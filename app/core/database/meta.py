from sqlalchemy import select
from sqlalchemy.orm import declarative_base
from sqlalchemy.schema import MetaData
from sqlalchemy.ext.asyncio import AsyncSession
from ..database import db_session


NAMING_CONVENTION = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}
metadata = MetaData(naming_convention=NAMING_CONVENTION)


class OrmBaseModel:

    @staticmethod
    def db_session() -> AsyncSession:
        return db_session()

    async def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
        await self.db_session.flush()

    async def delete(self):
        await self.db_session.delete(self)
        await self.db_session.flush()

    @classmethod
    async def get_by_id(cls, _id, id_column='id'):
        column = getattr(cls, id_column)
        return (await cls.db_session.scalars(
            select(cls)
            .where(column == _id)
        )).one_or_none()


OrmBaseModel = declarative_base(metadata=metadata, cls=OrmBaseModel)