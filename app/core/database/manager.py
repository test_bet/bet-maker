import typing
from collections import deque
from contextvars import ContextVar, Token
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.asyncio import AsyncSession, AsyncSessionTransaction
from app.utils import Singleton
from .exceptions import DBSessionException

DBSessionFactory = sessionmaker(class_=AsyncSession)


class DBSessionManager(metaclass=Singleton):

    def __init__(self):
        self.__connection_ctx_var: ContextVar[typing.Optional[typing.Dict]] = ContextVar(
            'current_db_connection', default={
                'session': None,
                'transactions': deque(),
            }
        )

    @property
    def session(self) -> typing.Optional[AsyncSession]:
        return self.__connection_ctx_var.get().get('session', None)

    @property
    def _transactions(self) -> typing.Deque[AsyncSessionTransaction]:
        return self.__connection_ctx_var.get().get('transactions')

    async def connect(self) -> Token:
        return self.__connection_ctx_var.set({
            'session': DBSessionFactory(),
            'transactions': deque()
        })

    async def disconnect(self, token: Token):
        if self.session is None:
            raise DBSessionException('Database is not connected')

        await self.session.close()
        self.__connection_ctx_var.reset(token)

    async def transaction(self, auto_start: bool = True) -> AsyncSessionTransaction:
        if not self.session:
            raise DBSessionException('Database is not connected')

        if not self._transactions:
            transaction = self.session.begin()
        else:
            transaction = self.session.begin_nested()

        self._transactions.append(transaction)
        if auto_start:
            await transaction
        return transaction

    async def __aenter__(self) -> AsyncSessionTransaction:
        transaction = await self.transaction(auto_start=False)
        return await transaction.__aenter__()

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        transaction = self._transactions.pop()
        await transaction.__aexit__(exc_type, exc_val, exc_tb)


def db_session() -> AsyncSession:
    return DBSessionManager().session
