from pydantic import BaseModel
from .utils import RecursiveGetterDict
from app.utils import to_camel_case


class PydanticBaseSchema(BaseModel):
    class Config:
        orm_mode = True
        getter_dict = RecursiveGetterDict
        alias_generator = to_camel_case
        allow_population_by_field_name = True
