from typing import Any
from collections.abc import Mapping
from pydantic.utils import GetterDict


class RecursiveGetterDict(GetterDict):

    def _getattr(self, key: Any, default: Any = None):
        value = getattr(self._obj, key, default)
        if isinstance(value, type) and not isinstance(value, Mapping):
            value = RecursiveGetterDict(value)
        return value

    def __getitem__(self, key: str) -> Any:
        try:
            return self._getattr(key)
        except AttributeError as e:
            raise KeyError(key) from e

    def get(self, key: Any, default: Any = None) -> Any:
        return self._getattr(key, default)
