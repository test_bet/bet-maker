from httpx import AsyncClient


class HTTPXClientWrapper:
    async_client = None

    def start(self):
        self.async_client = AsyncClient()

    async def stop(self):
        await self.async_client.aclose()

    def __call__(self):
        return self.async_client


httpx_client_wrapper = HTTPXClientWrapper()
