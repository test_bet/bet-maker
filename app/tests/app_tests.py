import time

import pytest as pytest

from datetime import datetime, timedelta
from uuid import uuid4
from app.models.enums import BetState
from app.schemas import EventState
from app.utils import httpx_client_wrapper
from app.settings import LINE_PROVIDER_URL

base_url = 'http://localhost:8000/api'


@pytest.mark.parametrize('anyio_backend', ['asyncio'])
async def test_simple_workflow(anyio_backend):
    httpx_client_wrapper.start()
    async_client = httpx_client_wrapper()
    test_event_id = str(uuid4())

    test_event = {
        'event_id': test_event_id,
        'coefficient': 1.0,
        'deadline': (datetime.utcnow() + timedelta(seconds=600)).strftime('%Y-%m-%dT%H:%M:%S%z'),
        'state': EventState.NEW.value
    }

    test_bet = {
        "eventId": test_event_id,
        "amount": 1234567978978,
        "bet": "FINISHED_WIN"
    }

    create_response = await async_client.put(LINE_PROVIDER_URL + '/event', json=test_event)
    assert create_response.status_code == 201

    create_response = await async_client.put(base_url + '/bet', json=test_bet)
    assert create_response.status_code == 201
    test_bet_id = create_response.json()['id']

    test_event['state'] = EventState.FINISHED_WIN.value
    update_response = await async_client.post(LINE_PROVIDER_URL + '/event', json=test_event)
    assert update_response.status_code == 200

    time.sleep(1)

    response = await async_client.get(base_url + '/bets')
    assert response.status_code == 200

    bets = response.json()['bets']

    bet_count = 0
    for bet in bets:
        if bet['id'] == test_bet_id:
            assert bet['state'] == BetState.FINISHED_WIN.value
            bet_count += 1

    assert bet_count == 1

    await httpx_client_wrapper.stop()
