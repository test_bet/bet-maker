import os

DEPLOY_ENV = os.getenv('DEPLOY_ENV', 'local')
URL_PREFIX = os.getenv('URL_PREFIX', '/api').rstrip('/')
DATABASE_URL = os.getenv('DATABASE_URL')
LINE_PROVIDER_URL = os.getenv('LINE_PROVIDER_URL', 'http://line-provider:8080')


def debug_mode() -> bool:
    return DEPLOY_ENV == 'local'
