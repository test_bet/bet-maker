from enum import Enum


class BetState(Enum):
    NOT_PLAY = 'NOTPLAY'
    FINISHED_WIN = 'FINISHED_WIN'
    FINISHED_LOSE = 'FINISHED_LOSE'

    def __str__(self):
        return self.value


class Bet(Enum):
    FINISHED_WIN = 'FINISHED_WIN'
    FINISHED_LOSE = 'FINISHED_LOSE'

    def __str__(self):
        return self.value
