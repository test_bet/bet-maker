import sqlalchemy as sa

from sqlalchemy.dialects.postgresql import UUID
from app.core import OrmBaseModel
from app.models.mixin import UuidIdMixin, TimestampMixin
from app.models.enums import BetState, Bet


class BetModel(UuidIdMixin, TimestampMixin, OrmBaseModel):
    __tablename__ = 'bet_table'

    event_id = sa.Column(UUID(as_uuid=True), nullable=False)
    amount = sa.Column(sa.Float, nullable=False)
    coefficient = sa.Column(sa.Float, nullable=False)
    bet = sa.Column(sa.Enum(*[x.value for x in Bet], name='bet'), nullable=False)

    state = sa.Column(sa.Enum(*[x.value for x in BetState], name='state'), nullable=False,
                      default=BetState.NOT_PLAY.value)
