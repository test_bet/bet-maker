from datetime import datetime
from decimal import Decimal
from enum import Enum
from typing import List
from uuid import UUID
from app.core import PydanticBaseSchema


class EventState(Enum):
    NEW = "NEW"
    FINISHED_WIN = "FINISHED_WIN"
    FINISHED_LOSE = "FINISHED_LOSE"


class EventSchema(PydanticBaseSchema):
    event_id: UUID
    coefficient: Decimal = None
    deadline: datetime = None
    state: EventState = None


class EventsSchema(PydanticBaseSchema):
    events: List[EventSchema]


class ChangeEventSchema(PydanticBaseSchema):
    event_id: UUID
    state: EventState
