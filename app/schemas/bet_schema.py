from datetime import datetime
from typing import List
from uuid import UUID
from pydantic import condecimal
from decimal import Decimal
from app.core import PydanticBaseSchema
from app.models.enums import Bet


class BetInSchema(PydanticBaseSchema):
    event_id: UUID
    amount: condecimal(gt=Decimal(0), decimal_places=2)
    bet: Bet


class BetBaseOutSchema(PydanticBaseSchema):
    id: UUID


class BetOutSchema(BetBaseOutSchema):
    event_id: UUID
    created_at: datetime
    coefficient: float
    amount: float
    bet: str
    state: str


class BetOutHistorySchema(PydanticBaseSchema):
    bets: List[BetOutSchema]
