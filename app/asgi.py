from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from sqlalchemy.ext.asyncio import create_async_engine

from app.settings import debug_mode, DATABASE_URL, URL_PREFIX
from app.endpoints import router
from app.core.database import DBSessionFactory
from app.middleware import middleware
from app.utils import httpx_client_wrapper

engine = create_async_engine(DATABASE_URL)

app = FastAPI(
    debug=debug_mode(),
    middleware=middleware,
    exception_handlers={
        RequestValidationError: None,
    },
    openapi_url='{}/openapi.json'.format(URL_PREFIX),
    docs_url='{}/docs'.format(URL_PREFIX),
    redoc_url='{}/redoc'.format(URL_PREFIX),
    swagger_ui_oauth2_redirect_url='{}/docs/oauth2-redirect'.format(URL_PREFIX),
)

app.include_router(router, prefix=URL_PREFIX)


@app.on_event('startup')
async def on_startup():
    DBSessionFactory.configure(bind=engine)
    httpx_client_wrapper.start()


@app.on_event('shutdown')
async def on_shutdown():
    await engine.dispose()
    await httpx_client_wrapper.stop()
