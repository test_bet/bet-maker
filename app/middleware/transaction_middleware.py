from starlette.types import ASGIApp, Receive, Scope, Send
from app.core import DBSessionManager


class TransactionMiddleware:

    def __init__(self, app: ASGIApp):
        self.app = app

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        if scope['type'] not in ['http']:
            await self.app(scope, receive, send)
            return

        token = await DBSessionManager().connect()
        try:
            async with DBSessionManager():
                await self.app(scope, receive, send)
        finally:
            await DBSessionManager().disconnect(token)
