from starlette.middleware import Middleware
from .transaction_middleware import TransactionMiddleware
# from uvicorn.middleware.proxy_headers import ProxyHeadersMiddleware

middleware = [
    # Middleware(ProxyHeadersMiddleware, trusted_hosts='*'),
    Middleware(TransactionMiddleware),
]
