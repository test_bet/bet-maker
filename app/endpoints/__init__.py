from fastapi import APIRouter, Depends, HTTPException
from httpx import AsyncClient
from app.models import BetModel
from app.models.enums import BetState
from app.schemas import BetInSchema, ChangeEventSchema, BetBaseOutSchema, EventsSchema, BetOutHistorySchema
from app.core import db_session
from app.settings import LINE_PROVIDER_URL
from sqlalchemy import select

from app.utils import httpx_client_wrapper

router = APIRouter()


@router.get('/events')
async def get_events():
    async_client = httpx_client_wrapper()
    response = await async_client.get(LINE_PROVIDER_URL + '/events')
    return EventsSchema(**response.json())


@router.put('/bet', response_model=BetBaseOutSchema, status_code=201)
async def create_bet(bid: BetInSchema):
    async_client = httpx_client_wrapper()
    response = await async_client.get(LINE_PROVIDER_URL + f'/event/{str(bid.event_id)}')
    if response.status_code == 200:
        bet_model = BetModel(event_id=bid.event_id,
                             amount=bid.amount,
                             bet=bid.bet.value,
                             coefficient=response.json()['coefficient'])
        db_session().add(bet_model)
        await db_session().flush()
        return BetBaseOutSchema(id=bet_model.id)

    raise HTTPException(status_code=404, detail="Event not found")


@router.post('/change-status-bet', status_code=200)
async def change_status_bet(change_event: ChangeEventSchema):
    bets = (await db_session().scalars(select(BetModel).where(
            BetModel.event_id == change_event.event_id,
            BetModel.state == BetState.NOT_PLAY.value,
            ))).all()

    for bet in bets:
        if bet.bet == change_event.state.value:
            bet.state = BetState.FINISHED_WIN.value
        else:
            bet.state = BetState.FINISHED_LOSE.value

        db_session().add(bet)


@router.get('/bets', status_code=200)
async def bets_history():
    return BetOutHistorySchema(bets=(await db_session().scalars(select(BetModel))).all())



