"""Create schema

Revision ID: dcd5199e3364
Revises: 
Create Date: 2023-03-16 20:05:52.157036

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'dcd5199e3364'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('bet_table',
    sa.Column('id', postgresql.UUID(as_uuid=True), nullable=False),
    sa.Column('created_at', sa.DateTime(), server_default=sa.text('now()'), nullable=False),
    sa.Column('updated_at', sa.DateTime(), server_default=sa.text('now()'), nullable=False),
    sa.Column('event_id', postgresql.UUID(as_uuid=True), nullable=False),
    sa.Column('amount', sa.Float(), nullable=False),
    sa.Column('coefficient', sa.Float(), nullable=False),
    sa.Column('bet', sa.Enum('FINISHED_WIN', 'FINISHED_LOSE', name='bet'), nullable=False),
    sa.Column('state', sa.Enum('NOTPLAY', 'FINISHED_WIN', 'FINISHED_LOSE', name='state'), nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_bet_table'))
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('bet_table')
    # ### end Alembic commands ###
